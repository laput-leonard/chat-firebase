import React, { Component } from 'react';
import { Button } from 'semantic-ui-react';

class Header extends Component {
	render() {
		const { currentUser, handleLogout, toggleLoginModal, toggleSignupModal } = this.props;
		console.log(currentUser);
		return (
			<div className="chat-header">
				{currentUser && (
					<React.Fragment>
						<Button onClick={() => handleLogout()}>Logout</Button>
					</React.Fragment>
				)}
				{!currentUser && (
					<React.Fragment>
						<Button onClick={() => toggleLoginModal()}>Login</Button>
						<Button onClick={() => toggleSignupModal()}>Sign Up</Button>
					</React.Fragment>
				)}
			</div>
		);
	}
}

export default Header;
