import React, { Component } from 'react';
import { Modal, Form, Message } from 'semantic-ui-react';

class ModalSignup extends Component {
	render() {
		const { signupForm, openSignup, loadingSignup, toggleSignupModal, handleSubmitSignupForm, handleChangeFieldSignupForm } = this.props;

		return (
			<Modal size="tiny" open={openSignup} closeIcon onClose={() => toggleSignupModal()}>
				<Modal.Header>Sign Up</Modal.Header>
				<Modal.Content>
					<Form onSubmit={handleSubmitSignupForm}>
						<Form.Input
							fluid
							type="text"
							name="display_name"
							value={signupForm.display_name}
							label="Display Name"
							placeholder="Enter Display Name"
							required
							onChange={handleChangeFieldSignupForm}
						/>
						<Form.Input
							fluid
							type="email"
							name="email"
							value={signupForm.email}
							label="Email"
							placeholder="Enter Email Address"
							required
							onChange={handleChangeFieldSignupForm}
						/>
						<Form.Input
							fluid
							type="password"
							name="password"
							value={signupForm.password}
							label="Password"
							placeholder="Enter Password"
							required
							onChange={handleChangeFieldSignupForm}
						/>
						<Form.Button primary type="submit" disabled={loadingSignup} loading={loadingSignup}>
							Sign Up
						</Form.Button>
					</Form>
					{signupForm.error && <Message error header="Error" content={signupForm.error} />}
				</Modal.Content>
			</Modal>
		);
	}
}

export default ModalSignup;
