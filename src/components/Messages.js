import React, { Component } from 'react';
import moment from 'moment';
import { Feed } from 'semantic-ui-react';

const avatarSrc = 'https://react.semantic-ui.com/images/avatar/small/matthew.png';
class Messages extends Component {
	render() {
		const { currentUser, messages } = this.props;

		return (
			<Feed>
				{messages.map(message => {
					const data = message.data();
					const isCurrentUserMessage = currentUser && currentUser.uid.toString() === data.uid.toString();
					let agoText = '';
					const now = moment(moment.now());
					const timePosted = moment(data.timestamp);
					const imageSrc = currentUser.photoURL || avatarSrc;

					if (now.diff(timePosted, 'hours') > 0) {
						const hh = now.diff(timePosted, 'hours');
						agoText = `${hh} hour${hh > 1 ? 's' : ''} ago`;
					} else if (now.diff(timePosted, 'minutes') > 0) {
						const mm = now.diff(timePosted, 'minutes');
						agoText = `${mm} minute${mm > 1 ? 's' : ''} ago`;
					} else if (now.diff(timePosted, 'seconds') > 0) {
						agoText = `Just now`;
					} else {
						agoText = `Just now`;
					}

					return (
						<Feed.Event key={message.id} className={isCurrentUserMessage ? 'user' : ''}>
							<Feed.Label image={imageSrc} />
							<Feed.Content>
								<Feed.Summary>
									<Feed.User>{data.display_name}</Feed.User>
									<Feed.Date>{agoText}</Feed.Date>
								</Feed.Summary>
								<Feed.Extra text>{data.message}</Feed.Extra>
							</Feed.Content>
						</Feed.Event>
					);
				})}
			</Feed>
		);
	}
}

export default Messages;
