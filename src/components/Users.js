import React, { Component } from 'react';
import { List, Image, Label } from 'semantic-ui-react';

const avatarSrc = 'https://react.semantic-ui.com/images/avatar/small/matthew.png';
class Users extends Component {
	render() {
		const { currentUser, users } = this.props;

		return (
			<List verticalAlign="middle">
				{users.map(user => {
					const data = user.data();
					const color = data.logged_in ? 'green' : 'grey';
					const headerAs = currentUser && user.id.toString() === currentUser.uid.toString() ? 'a' : '';

					return (
						<List.Item key={user.id}>
							<Image avatar src={avatarSrc} />
							<List.Content>
								<List.Header as={headerAs}>{data.display_name}</List.Header>
								<Label circular color={color} empty />
							</List.Content>
						</List.Item>
					);
				})}
			</List>
		);
	}
}

export default Users;
