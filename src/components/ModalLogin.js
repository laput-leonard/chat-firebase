import React, { Component } from 'react';
import { Modal, Form, Message, Button, Divider } from 'semantic-ui-react';

class ModalLogin extends Component {
	render() {
		const {
			loginForm,
			openLogin,
			loadingLogin,
			toggleLoginModal,
			handleSubmitLoginForm,
			handleChangeFieldLoginForm,
			handleLoginWithGoogle
		} = this.props;

		return (
			<Modal size="tiny" open={openLogin} closeIcon onClose={() => toggleLoginModal()}>
				<Modal.Header>Login</Modal.Header>
				<Modal.Content>
					<Button color="green" icon="google" content="Sign in with Google" onClick={handleLoginWithGoogle} />
					<Divider />
					<Form onSubmit={handleSubmitLoginForm}>
						<Form.Input
							fluid
							type="email"
							name="email"
							value={loginForm.email}
							label="Email"
							placeholder="Enter Email Address"
							required
							onChange={handleChangeFieldLoginForm}
						/>
						<Form.Input
							fluid
							type="password"
							name="password"
							value={loginForm.password}
							label="Password"
							placeholder="Enter Password"
							required
							onChange={handleChangeFieldLoginForm}
						/>
						<Form.Button primary type="submit" disabled={loadingLogin} loading={loadingLogin}>
							Login
						</Form.Button>
					</Form>
					{loginForm.error && <Message error header="Error" content={loginForm.error} />}
				</Modal.Content>
			</Modal>
		);
	}
}

export default ModalLogin;
