import * as firebase from 'firebase';

const config = {
	apiKey: 'AIzaSyCMFstsS4KeQpSrJzBcmRckkxIeFxR03JA',
	authDomain: 'chat-firebase-70aac.firebaseapp.com',
	databaseURL: 'https://chat-firebase-70aac.firebaseio.com',
	projectId: 'chat-firebase-70aac',
	storageBucket: 'chat-firebase-70aac.appspot.com',
	messagingSenderId: '426246411028',
	appId: '1:426246411028:web:e52752b33f4f30fd'
};

firebase.initializeApp(config);

// Make auth and firestore references
const auth = firebase.auth();
const db = firebase.firestore();
const functions = firebase.functions();
const googleProvider = new firebase.auth.GoogleAuthProvider();

export { auth, db, functions, googleProvider };
