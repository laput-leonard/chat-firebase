import React, { Component } from 'react';
import { auth, db, googleProvider } from './firebase';
import { Grid, Form } from 'semantic-ui-react';
import Header from './components/Header';
import Users from './components/Users';
import Messages from './components/Messages';
import ModalLogin from './components/ModalLogin';
import ModalSignup from './components/ModalSignup';

class App extends Component {
	constructor(props) {
		super(props);

		this.state = {
			currentUser: null,
			users: [],
			message: '',
			messages: [],
			loginForm: {
				email: '',
				password: '',
				error: ''
			},
			signupForm: {
				display_name: '',
				email: '',
				password: '',
				error: ''
			},
			openLogin: false,
			openSignup: false,
			loadingUsers: false,
			loadingLogin: false,
			loadingSignup: false
		};
		const self = this;

		// Auth event listeners
		auth.onAuthStateChanged(currentUser => {
			self.setState({ currentUser });
		});
	}

	componentDidMount() {
		const self = this;

		// Get users
		db.collection('users')
			.orderBy('logged_in', 'desc') // show logged in first
			.onSnapshot(
				snapshot => {
					self.setState({ users: snapshot.docs });
				},
				error => {
					console.log(error);
				}
			);

		// Get message
		db.collection('messages')
			.orderBy('timestamp', 'asc')
			.limit(100)
			.onSnapshot(
				snapshot => {
					self.setState({ messages: snapshot.docs });
				},
				error => {
					console.log(error);
				}
			);

		this.scrollToBottom();
	}

	componentDidUpdate() {
		this.scrollToBottom();
	}

	toggleLoginModal = () => {
		const { openLogin } = this.state;
		this.setState({ openLogin: !openLogin });
	};

	toggleSignupModal = () => {
		const { openSignup } = this.state;
		this.setState({ openSignup: !openSignup });
	};

	handleChangeFieldLoginForm = (event, data) => {
		const newLoginForm = { ...this.state.loginForm };
		const { name, value } = data;

		newLoginForm[name] = value;
		this.setState({ loginForm: newLoginForm });
	};

	handleChangeFieldSignupForm = (event, data) => {
		const newSignupForm = { ...this.state.signupForm };
		const { name, value } = data;

		newSignupForm[name] = value;
		this.setState({ signupForm: newSignupForm });
	};

	handleChangeMessageForm = (event, data) => {
		this.setState({ message: data.value });
	};

	handleSubmitLoginForm = () => {
		const self = this;
		const { loginForm } = this.state;
		const newLoginForm = { ...loginForm };
		const { email, password } = loginForm;

		// Reset error
		newLoginForm.error = '';
		this.setState({ loadingLogin: true, loginForm: newLoginForm });

		auth
			.signInWithEmailAndPassword(email, password)
			.then(async credentials => {
				const { user } = credentials;
				await db
					.collection('users')
					.doc(user.uid)
					.update({
						logged_in: true
					});
				self.setState({ loadingLogin: false });
				self.toggleLoginModal();
				console.log('User logged in.');
			})
			.catch(error => {
				console.log(error);

				newLoginForm.error = error.message;
				self.setState({ loadingLogin: false, loginForm: newLoginForm });
			});
	};

	handleSubmitSignupForm = () => {
		const self = this;
		const { signupForm } = this.state;
		const newSignupForm = { ...signupForm };
		const { display_name, email, password } = signupForm;

		// Reset error
		newSignupForm.error = '';
		this.setState({ loadingSignup: true, signupForm: newSignupForm });

		auth
			.createUserWithEmailAndPassword(email, password)
			.then(async credentials => {
				const { user } = credentials;
				await user.updateProfile({ displayName: display_name });
				await db
					.collection('users')
					.doc(user.uid)
					.set({
						display_name,
						logged_in: true
					});
				self.setState({ loadingSignup: false });
				self.toggleSignupModal();
				console.log('User signed up.');
			})
			.catch(error => {
				console.log(error);

				newSignupForm.error = error.message;
				self.setState({ loadingSignup: false, signupForm: newSignupForm });
			});
	};

	handleLogout = () => {
		const { currentUser } = this.state;
		const { uid } = currentUser;

		auth
			.signOut()
			.then(async () => {
				await db
					.collection('users')
					.doc(uid)
					.update({
						logged_in: false
					});
				console.log('User logged out.');
			})
			.catch(error => {
				console.log(error);
			});
	};

	handleSubmitMessage = () => {
		const self = this;
		const { currentUser, message } = this.state;
		const newMessage = message;

		self.setState({ message: '' });
		db.collection('messages')
			.add({
				uid: currentUser.uid,
				display_name: currentUser.displayName,
				message: newMessage,
				timestamp: +new Date()
			})
			.then(() => {
				console.log('Message created.', newMessage);
			})
			.catch(error => {
				console.log(error);
			});
	};

	handleLoginWithGoogle = () => {
		const self = this;

		auth
			.signInWithPopup(googleProvider)
			.then(async credentials => {
				const { user } = credentials;
				const result = await db
					.collection('users')
					.doc(user.uid)
					.get();

				// Create user
				if (!result.exists) {
					await db
						.collection('users')
						.doc(user.uid)
						.set({
							display_name: user.displayName,
							logged_in: true
						});
				}

				self.toggleLoginModal();
				console.log('User logged in with Google.');
			})
			.catch(error => {
				console.log(error);
			});
	};

	scrollToBottom = () => {
		this.messagesEnd.scrollIntoView({ behavior: 'smooth' });
	};

	render() {
		const { currentUser, users, message, messages, loginForm, signupForm, openLogin, openSignup, loadingLogin, loadingSignup } = this.state;

		return (
			<React.Fragment>
				<Header
					currentUser={currentUser}
					handleLogout={this.handleLogout}
					toggleLoginModal={this.toggleLoginModal}
					toggleSignupModal={this.toggleSignupModal}
				/>

				<h1>TEST</h1>
				<Grid>
					<Grid.Column width={3} className="chat-content-users">
						<Users currentUser={currentUser} users={users} />
					</Grid.Column>
					<Grid.Column width={13} className="chat-content-messages">
						<div className="messages-container">
							<Messages currentUser={currentUser} messages={messages} />
							<div
								style={{ float: 'left', clear: 'both' }}
								ref={el => {
									this.messagesEnd = el;
								}}
							/>
						</div>
						<Form onSubmit={this.handleSubmitMessage} size="big">
							{currentUser && (
								<Form.Group>
									<Form.Input
										width={14}
										name="message"
										value={message}
										placeholder="Enter Message"
										required
										onChange={this.handleChangeMessageForm}
									/>
									<Form.Button fluid size="big" width={2} primary type="submit">
										Send
									</Form.Button>
								</Form.Group>
							)}
							{!currentUser && <div>Please login to message.</div>}
						</Form>
					</Grid.Column>
				</Grid>

				<ModalLogin
					loginForm={loginForm}
					openLogin={openLogin}
					loadingLogin={loadingLogin}
					toggleLoginModal={this.toggleLoginModal}
					handleChangeFieldLoginForm={this.handleChangeFieldLoginForm}
					handleSubmitLoginForm={this.handleSubmitLoginForm}
					handleLoginWithGoogle={this.handleLoginWithGoogle}
				/>

				<ModalSignup
					signupForm={signupForm}
					openSignup={openSignup}
					loadingSignup={loadingSignup}
					toggleSignupModal={this.toggleSignupModal}
					handleChangeFieldSignupForm={this.handleChangeFieldSignupForm}
					handleSubmitSignupForm={this.handleSubmitSignupForm}
				/>
			</React.Fragment>
		);
	}
}

export default App;
